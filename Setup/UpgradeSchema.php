<?php

namespace Froogal\Magento\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), "1.1.6", "<")) {

            $tables = ['sales_order', 'sales_invoice', 'sales_creditmemo'];

            foreach ($tables as $table) {
                $setup->getConnection()
                    ->addColumn(
                        $setup->getTable($table),
                        'redeemed_loyalty_points',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BIGINT,
                            'default' => 0,
                            'nullable' => true,
                            'comment' => 'Redeemed Loyalty Points'
                        ]
                    );

                $setup->getConnection()
                    ->addColumn(
                        $setup->getTable($table),
                        'loyalty_coupon_discount_type',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'nullable' => true,
                            'comment' => 'loyalty coupon discount type'
                        ]
                    );
                $setup->getConnection()
                    ->addColumn(
                        $setup->getTable($table),
                        'loyalty_coupon_code',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'nullable' => true,
                            'comment' => 'loyalty coupon code'
                        ]
                    );

                $setup->getConnection()
                    ->addColumn(
                        $setup->getTable($table),
                        'loyalty_coupon_title',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'nullable' => true,
                            'comment' => 'loyalty coupon title'
                        ]
                    );    
            }

            $quoteTable = 'quote';
            $orderTable = 'sales_order';
            $invoiceTable = 'sales_invoice';
            $creditmemoTable = 'sales_creditmemo';

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'loyalty_discount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' => 'Loyalty Discount'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'redeemed_loyalty_points',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BIGINT,
                        'default' => 0,
                        'nullable' => true,
                        'comment' => 'Redeemed Loyalty Points'
                    ]
                );

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'loyalty_coupon_discount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' => 'Loyalty Discount'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'loyalty_coupon_discount_type',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Redeemed Loyalty Points'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'loyalty_coupon_code',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Redeemed Loyalty Points'
                    ]
                );

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($quoteTable),
                    'loyalty_coupon_title',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'loyalty coupon title'
                    ]
                );    

            //Order tables
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'loyalty_discount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' => 'Loyalty Discount'
                    ]
                );

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'loyalty_coupon_discount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' => 'Loyalty Coupon Discount'
                    ]
                );


            //Invoice tables
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($invoiceTable),
                    'loyalty_discount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' => 'Loyalty Discount'
                    ]
                );

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($invoiceTable),
                    'loyalty_coupon_discount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' => 'Loyalty Coupon Discount'
                    ]
                );

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($invoiceTable),
                    'loyalty_coupon_title',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'loyalty coupon title'
                    ]
                );    

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($orderTable),
                    'loyalty_coupon_title',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'loyalty coupon title'
                    ]
                );                

            //Credit memo tables
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($creditmemoTable),
                    'loyalty_discount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' => 'Loyalty Discount'
                    ]
                );

            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($creditmemoTable),
                    'loyalty_coupon_discount',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        '10,2',
                        'default' => 0.00,
                        'nullable' => true,
                        'comment' => 'Loyalty Coupon Discount'
                    ]
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable($creditmemoTable),
                    'loyalty_coupon_title',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'loyalty coupon title'
                    ]
                );    
        }

        if (version_compare($context->getVersion(), "1.1.9", "<")) {

            $salesordergrid = 'sales_order_grid';

            $setup->getConnection()->addColumn(
                    $setup->getTable($salesordergrid),
                    'region_state',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 100,
                        'nullable' => true,
                        'comment' =>'State'
                    ]
                );
                $setup->getConnection()->addColumn(
                    $setup->getTable($salesordergrid),
                    'city',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 100,
                        'nullable' => true,
                        'comment' =>'City'
                    ]
                );
                $setup->getConnection()->addColumn(
                    $setup->getTable($salesordergrid),
                    'pin_code',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 10,
                        'nullable' => true,
                        'comment' =>'Pin Code'
                    ]
                );
        

            $salesorder = 'sales_order';
            //Customer table
           
                    $setup->getConnection()->addColumn(
                        $setup->getTable($salesorder),
                        'region_state',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 100,
                            'nullable' => true,
                            'comment' =>'State'
                        ]
                    );
                    $setup->getConnection()->addColumn(
                        $setup->getTable($salesorder),
                        'city',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 100,
                            'nullable' => true,
                            'comment' =>'City'
                        ]
                    );
                    $setup->getConnection()->addColumn(
                        $setup->getTable($salesorder),
                        'pin_code',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 10,
                            'nullable' => true,
                            'comment' =>'Pin Code'
                        ]
                    );
        }

        $setup->endSetup();


    }
}