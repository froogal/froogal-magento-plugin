<?php


namespace Froogal\Magento\Setup;

use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Customer\Api\CustomerMetadataInterface;

class UpgradeData implements UpgradeDataInterface
{

    private $customerSetupFactory;

    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context){

        $setup->startSetup();

        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $eavSetup = $this->eavSetupFactory
        ->create(['setup' => $setup]);

        $customerEntity = $customerSetup
        ->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity
        ->getDefaultAttributeSetId();

     $attributeSet = $this->attributeSetFactory->create();

     $attributeGroupId = $attributeSet
        ->getDefaultGroupId($attributeSetId);

        if (version_compare($context->getVersion(), '1.1.8') < 0) {

            $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'froogal_phone_number', [
                'type' => 'varchar',
                'label' => 'Phone Number',
                'input' => 'text',
                'source' => '',
                'required' => false,
                'visible' => true,
                'position' => 999,
                'system' => false,
                'backend' => \Froogal\Magento\Model\Attribute\Backend\PhoneNumber::class,
                'default' => '',
                'searchable' => true,
                'filterable' => true,
                'comparable' => false,
                'unique' => true,
                'user_defined' => true
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'froogal_phone_number')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,

                    'used_in_forms' => [
                    'adminhtml_customer',
                    'adminhtml_checkout',
                    'customer_account_create',
                    'customer_account_edit'
                ]]);
                $eavSetup->addAttributeToSet(
                    CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
                    CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
                    null,
                   'froogal_phone_number');
            $attribute->save();      


            $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'froogal_auth_token', [
                'type' => 'varchar',
                'label' => 'Auth Token',
                'input' => 'text',
                'source' => '',
                'required' => false,
                'visible' => false,
                'position' => 999,
                'system' => false,
                'backend' => ''
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'froogal_auth_token')
                ->addData(['used_in_forms' => [
                    'adminhtml_customer',
                    'adminhtml_checkout',
                    'customer_account_create',
                    'customer_account_edit'
                ]]);
            $attribute->save();


        }
        if (version_compare($context->getVersion(), '1.1.4') < 0) {

            $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'froogal_referral_code', [
                'type' => 'varchar',
                'label' => 'Referral Code',
                'input' => 'text',
                'source' => '',
                'required' => false,
                'visible' => true,
                'position' => 1001,
                'system' => false,
                'backend' => ''
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'froogal_referral_code')
                ->addData(['used_in_forms' => [
                    'adminhtml_customer',
                    'adminhtml_checkout',
                    'customer_account_create',
                    'customer_account_edit'
                ]]);
            $attribute->save();


        }
    }
}