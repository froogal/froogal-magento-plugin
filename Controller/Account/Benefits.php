<?php

namespace Froogal\Magento\Controller\Account;
use Magento\Framework\App\Config\ScopeConfigInterface;


class Benefits extends \Magento\Framework\App\Action\Action
{
    /*
     * Redirect behaviour
     */
    const REDIRECT_BENEFITS_ENABLED = '1';
    const REDIRECT_BENEFITS_DISABLED = '0';

     /*
     * Configuration
     */
    const REDIRECT_BENEFITS_CONFIG = 'froogalascendconfiguration/loyalty_points_config/benefits_active';

    protected $resultPageFactory;
    protected $resultRedirectFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
        )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    public function execute()
    {
        if (self::REDIRECT_BENEFITS_ENABLED === $this->scopeConfig->getValue(self::REDIRECT_BENEFITS_CONFIG)) 
        {            
            return $this->resultPageFactory->create();
        }
        else
        {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('404');
            return $resultRedirect;        
        }
       
    }
}
?>