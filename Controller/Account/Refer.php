<?php

namespace Froogal\Magento\Controller\Account;
use Magento\Framework\App\Config\ScopeConfigInterface;


class Refer extends \Magento\Framework\App\Action\Action
{
    /*
     * Redirect behaviour
     */
    const REDIRECT_REFERRAL_ENABLED = '1';
    const REDIRECT_REFERRAL_DISABLED = '0';

     /*
     * Configuration
     */
    const REDIRECT_REFERRAL_CONFIG = 'froogalascendconfiguration/loyalty_points_config/referral_active';

    protected $resultPageFactory;
    protected $resultRedirectFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
        )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    public function execute()
    {
        if (self::REDIRECT_REFERRAL_ENABLED === $this->scopeConfig->getValue(self::REDIRECT_REFERRAL_CONFIG)) 
        {            
            return $this->resultPageFactory->create();
        }
        else
        {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('404');
            return $resultRedirect;        
        }
       
    }
}
?>