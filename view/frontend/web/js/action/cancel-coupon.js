/**
 * Customer store credit(balance) application
 */
/*global define,alert*/
define(
    [
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/resource-url-manager',
        'Magento_Checkout/js/model/error-processor',
        'Magento_SalesRule/js/model/payment/discount-messages',
        'mage/storage',
        'Magento_Checkout/js/action/get-payment-information',
        'Magento_Checkout/js/model/totals',
        'mage/translate',
        'Froogal_Magento/js/action/reload-shipping-method',
        'Froogal_Magento/js/action/set-shipping-information',
        'Froogal_Magento/js/view/checkout/summary/loyalty-coupon-discount',
        'Magento_Checkout/js/model/cart/totals-processor/default',
        'Magento_Checkout/js/model/cart/cache',
    ],
    function ($, quote, urlManager, errorProcessor, messageContainer, storage, getPaymentInformationAction, totals, $t, reloadShippingMethod,setShippingInformationAction,loyaltyCouponDiscountAction,totalsProcessor,cartCache) {
        'use strict';

        return function (isApplied, isLoading) {
            var quoteId = quote.getQuoteId(),
                url = urlManager.getCancelCouponUrl(quoteId),
                message = $t('Your coupon was successfully removed.');
                setShippingInformationAction();
               
                   
            messageContainer.clear();

            return storage.delete(
                url,
                false
            ).done(
                function () {
                    var deferred = $.Deferred(); 
                    
                    totals.isLoading(true);
                   
                   
                        loyaltyCouponDiscountAction(); 
                        getPaymentInformationAction(deferred);
                        reloadShippingMethod();
                        
                        totalsProcessor.estimateTotals(quote.shippingAddress());

                    $.when(deferred).done(function () {
                        isApplied(false);                       
                        $('#ajax-loader3').hide();
                        $('#control_overlay_review').hide();
                    });
                    messageContainer.addSuccessMessage({
                        'message': message
                    });
                }
            ).fail(
                function (response) {
                    totals.isLoading(false);
                    errorProcessor.process(response, messageContainer);
                }
            ).always(
                function () {
                    isLoading(false);
                }
            );
        };
    }
);
