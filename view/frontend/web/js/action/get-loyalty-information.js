/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
 define([
    'jquery',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/url-builder',
    'mage/storage',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Customer/js/model/customer'
], function ($, quote, urlBuilder, storage, errorProcessor, customer) {
    'use strict';

    return function () {
        var serviceUrl;

        if (isCustomerLoggedIn) {
            serviceUrl = urlBuilder.createUrl('/loyalty/details', {});
            return storage.get(serviceUrl, false);
        } else  {
            let res = {success: false, message:"Customer not logged in",data:[]};
            return res;
        }
    };
});
