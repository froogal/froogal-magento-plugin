/**
 * Customer store credit(balance) application
 */
/*global define,alert*/
console.log('set coupon')
define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/resource-url-manager',
        'Magento_Checkout/js/model/error-processor',
        'Magento_SalesRule/js/model/payment/discount-messages',
        'mage/storage',
        'mage/translate',
        'Magento_Checkout/js/action/get-payment-information',
        'Magento_Checkout/js/model/totals',
        'Froogal_Magento/js/action/reload-shipping-method',
        'Magento_Checkout/js/model/payment/method-list',
        'Magento_Checkout/js/model/cart/totals-processor/default',
        'Magento_Checkout/js/model/cart/cache',
        'Magento_Checkout/js/action/set-shipping-information',
        'Froogal_Magento/js/view/checkout/summary/loyalty-coupon-discount'
    ],
    function (
        ko,
        $,
        quote,
        urlManager,
        errorProcessor,
        messageContainer,
        storage,
        $t,
        getPaymentInformationAction,
        totals,
        reloadShippingMethod,
        paymentMethodList,
        totalsProcessor,
        cartCache,
        setShippingInformationAction,
        loyaltyCouponDiscountAction
    ) {
        'use strict';
        return function (couponCode, isApplied, isLoading) {
            var quoteId = quote.getQuoteId();
            var url = urlManager.getApplyCouponUrl(couponCode, quoteId);
            var message = $t('Your coupon was successfully applied.');
            setShippingInformationAction(); 
            return storage.put(
                url,
                {},
                false
            ).done(
                function (response) {
                    console.log('set-coupon-code response',response);
                    if (response) {
                        var deferred = $.Deferred();
                       isLoading(false);
                        isApplied(true);
                        cartCache.clear('cartVersion');
                        // totals.isLoading(true);
                        
                        loyaltyCouponDiscountAction(); 
                        getPaymentInformationAction(deferred);
                        reloadShippingMethod();
                        
                        totalsProcessor.estimateTotals(quote.shippingAddress());
                       

                      
                       
                       
                        $.when(deferred).done(function () {
                            $('#ajax-loader3').hide();
                            $('#control_overlay_review').hide();
                        });
                        messageContainer.addSuccessMessage({'message': message});
                    }
                }
            ).fail(
                function (response) {
                    isLoading(false);
                    totals.isLoading(false);
                    $('#ajax-loader3').hide();
                    $('#control_overlay_review').hide();
                    errorProcessor.process(response, messageContainer);
                }
            );
        };
    }
);
