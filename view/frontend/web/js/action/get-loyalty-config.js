/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
 define([
    'jquery',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/url-builder',
    'mage/storage',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Customer/js/model/customer'
], function ($, quote, urlBuilder, storage, errorProcessor, customer) {
    'use strict';

    return async function (quoteId) {
        var serviceUrl;
        var quoteId = quote.getQuoteId();
        var params = {'quoteId':quoteId};
        /**
         * Loyalty for registered customer.
         */
        if (isCustomerLoggedIn) {
            serviceUrl = urlBuilder.createUrl('/loyalty/config', {});
            return await storage.post(serviceUrl, JSON.stringify(params) ,false);
        } else  {
            let res = {success: false, message:"Customer not logged in",data:[]};
            return res;
        }
    };
});
