/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

 define([
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote',
    'Froogal_Magento/js/action/get-loyalty-coupon-config',
    'Magento_Checkout/js/model/totals'
], function (Component, quote, getLoyaltyCouponConfigAction, totals) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Froogal_Magento/summary/discount'
        },
        // isDisplayed: ko.observable(false),
        initialize: function() {
            this._super();
            this.getModuleEnabled();
        //    this.newVar = ko.observable(this.getValue());


            return this;
        },
        totals: quote.getTotals(),
        isTaxDisplayedInGrandTotal: window.checkoutConfig.includeTaxInGrandTotal || false,

        /**
         * @return {*|Boolean}
         */
        isDisplayed: function () {
            return this.isFullMode() && this.getPureValue() != 0; //eslint-disable-line eqeqeq
        },
        getModuleEnabled: async function() {
            var res = await getLoyaltyCouponConfigAction();
            console.log('response data',res);
            this.isDisplayed(res.data && res.data.isModuleEnabled);
        },

        /**
         * @return {*}
         */
        getCouponCode: function () {           
            if (!this.totals()) {
                return null;
            }

            return this.totals()['coupon_code'];
        },

        /**
         * @return {*}
         */
        getCouponLabel: function () {
        
            if (!this.totals()) {
                return null;
            }

            return this.totals()['coupon_label'];
        },

        /**
         * Get discount title
         *
         * @returns {null|String}
         */
        getTitle: function () {
          
            var discountSegments;

            if (!this.totals()) {
                return null;
            }

            discountSegments = this.totals()['total_segments'].filter(function (segment) {
                return segment.code.indexOf('discount') !== -1;
            });
            console.log('discountseg',discountSegments);
          //  console.log('discountsegzero',discountSegments[0].title)

            return discountSegments.length ? discountSegments[0].title : null;
        },

        /**
         * @return {Number}
         */
        getPureValue: function () {
            var price = 0;
            console.log('get-pure-value',totals.getSegment('loyaltyCouponDiscount'));
            if (this.totals() && (totals.getSegment('loyaltyCouponDiscount').value > 0)) {
                price = totals.getSegment('loyaltyCouponDiscount') ? totals.getSegment('loyaltyCouponDiscount').value : null;
            }
            else if (this.totals() && this.totals()['discount_amount']) {
                price = parseFloat(this.totals()['discount_amount']);
            }
            console.log('price',price);
            return price;
        },

        /**
         * @return {*|String}
         */
        getValue: function () {
         
            return this.getFormattedPrice(this.getPureValue());
        }
    });
});
