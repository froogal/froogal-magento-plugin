define(
    [
        'jquery',
        'ko',
        'underscore',
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'Froogal_Magento/js/action/set-coupon-code',
        'Froogal_Magento/js/action/cancel-coupon',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/cart/totals-processor/default',
        'Magento_Checkout/js/model/cart/cache',
        // 'Froogal_Magento/js/view/payment/custom-selected-payment-method',
        'Magento_Checkout/js/model/payment/method-list',
        'Magento_Checkout/js/action/get-payment-information',
        'mage/adminhtml/events'
    ],
    function($, ko, _, Component, quote, setCouponCodeAction, cancelCouponAction, customer, totalsProcessor, cartCache, paymentMethodList, getPaymentInformation) {
        'use strict';
        var totals = quote.getTotals();
        var couponCode = ko.observable(null);
        if (totals()) {
            var ccode = window.checkoutConfig.quoteData.loyalty_coupon_code ? window.checkoutConfig.quoteData.loyalty_coupon_code : totals()['coupon_code'];
            couponCode(ccode);
        }
        var isApplied = ko.observable(couponCode() != null);
        var isLoading = ko.observable(false);
        var isLoggedIn = ko.observable(isCustomerLoggedIn);
        return Component.extend({
            defaults: {
                template: 'Froogal_Magento/payment/discount'
            },
            couponCode: couponCode,

            isShowDiscount: ko.observable(window.checkoutConfig.show_discount),
            /**
             * Applied flag
             */
            isApplied: isApplied,
            isLoading: isLoading,
            isLoggedIn: isLoggedIn,
            initialize: function() {
                this._super();
                _.bindAll(this, 'cancel');
                varienGlobalEvents.attachEventHandler('cart_item_delete', this.cancel);
                varienGlobalEvents.attachEventHandler('cart_item_qty_update', this.cancel);
                return this;
            },
            /**
             * Coupon code application procedure
             */
            apply: function() {
                alert('apply coupon');
                if (this.validate()) {
                    this.showOverlay();
                    isLoading(true);
                    setCouponCodeAction(couponCode(), isApplied, isLoading);
                }
            },
            /**
             * Cancel using coupon
             */
            cancel: function() {
                if (couponCode() || window.checkoutConfig.quoteData.loyalty_coupon_code) {
                    if (this.validate()) {
                        this.showOverlay();
                        isLoading(true);
                        couponCode('');
                        cancelCouponAction(isApplied, isLoading);
                        cartCache.clear('cartVersion');
                        totalsProcessor.estimateTotals(quote.shippingAddress());
                       
                    }
                }
            },

            showOverlay: function() {
                $('#ajax-loader3').show();
                $('#control_overlay_review').show();
            },

            hideOverlay: function() {
                $('#ajax-loader3').hide();
                $('#control_overlay_review').hide();
            },


            /**
             * Coupon form validation
             *
             * @returns {boolean}
             */
            validate: function() {
                var form = '#discount-form';
                return $(form).validation() && $(form).validation('isValid');
            }
        });
    }
);