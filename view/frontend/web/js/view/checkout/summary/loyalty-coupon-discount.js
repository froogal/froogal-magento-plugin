/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'ko',
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/model/totals',
        'Froogal_Magento/js/action/get-loyalty-coupon-config',
        'Magento_Checkout/js/action/set-shipping-information'
    ],
    function (ko, Component, quote, priceUtils, totals, getLoyaltyCouponConfigAction, SetShippingInformationAction) {
        "use strict";
        return Component.extend({
            defaults: {
                isFullTaxSummaryDisplayed: window.checkoutConfig.isFullTaxSummaryDisplayed || false,
                template: 'Froogal_Magento/checkout/summary/loyalty-coupon-discount'
            },
            isDisplayed: ko.observable(false),
            initialize: function () {
                this._super();
                this.getModuleEnabled();

                return this;
            },
            totals: quote.getTotals(),
            isTaxDisplayedInGrandTotal: window.checkoutConfig.includeTaxInGrandTotal || false,
            getModuleEnabled: async function() {
                console.log('module enabled');
                var res = await getLoyaltyCouponConfigAction();
                this.isDisplayed(res.data && res.data.isModuleEnabled);
                console.log('module enabled response',res);
                // console.log('module enabled response data',this.isDisplayed(res[2].isModuleEnabled));
            },
            
            getValue: function() {
                console.log('loyalty function');
                var price = 0;

               if(totals.getSegment('loyaltyCouponDiscount').value == '')
                {
                    console.log('loyalty coupon discount123',totals.getSegment('loyaltyCouponDiscount').value);

                    this.isDisplayed(false);
                }
                if (this.totals()) {
                    price = totals.getSegment('loyaltyCouponDiscount') ? totals.getSegment('loyaltyCouponDiscount').value : null;
                }
                console.log('displayed',this.isDisplayed);
                console.log('price',price);
                return this.getFormattedPrice(price);
            },
           
            getTitle: function() {
                var title = '';
                if (this.totals()) {
                    title = totals.getSegment('loyaltyCouponDiscount') ? totals.getSegment('loyaltyCouponDiscount').title : '';
                }
                return title;
            },
            getDiscountTitle: function() {
                var discounttitle = '';
                if (this.totals()) {
                    discounttitle = totals.getSegment('loyaltyCouponDiscount') ? totals.getSegment('loyaltyCouponDiscount').discounttitle : '';
                }
                return discounttitle;
            }
        });
    }
);