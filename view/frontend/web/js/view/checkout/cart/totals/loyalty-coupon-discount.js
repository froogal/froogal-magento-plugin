/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
 define(
    [
        'ko',
        'Froogal_Magento/js/view/checkout/summary/loyalty-coupon-discount',
        'Froogal_Magento/js/action/get-loyalty-config'
    ],
    function (ko, Component, getLoyaltyConfigAction) {
        'use strict';

        return Component.extend({

            /**
             * @override
             */
        });
    }
);