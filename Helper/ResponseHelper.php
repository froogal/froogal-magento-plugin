<?php

namespace Froogal\Magento\Helper;

class ResponseHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\CustomerFactory $customerFactory)
    {
        $this->customerFactory = $customerFactory;
        parent::__construct($context);
    }

    public function success($message,$data=[])
    {
        $arr = ['success' => true ,'message' =>$message,'data' =>$data];
        return $arr;
    }

    public function error($message,$errors=[])
    {
        $arr = ['success' => false ,'message' =>$message,'errors' => $errors];
        return $arr;
    }
}