<?php
namespace Froogal\Magento\Helper;

class Helpers extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\CustomerFactory $customerFactory)
    {
        $this->customerFactory = $customerFactory;
        parent::__construct($context);
    }

    public function success($message,$data=[])
    {
        $arr = ['success' => true ,'message' =>$message,'data' =>$data];
        return $arr;
    }

    public function error($message,$errors=[])
    {
        $arr = ['success' => false ,'message' =>$message,'errors' => $errors];
        return $arr;
    }
    public function isPhoneNumber($number)
    {
        $mobileregex = "/^[6-9][0-9]{9}$/" ;
        return preg_match($mobileregex, $number) ? true : false;
    }

    public function isEmail($email)
    {
        $emailRegex = "/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix";
        return preg_match($emailRegex, $email) ? true : false;
    }

    public function isPhoneNumberExists($number)
    {
       try
       {
           $customerFactory = $this->customerFactory->create();
           $customerCollection = $customerFactory->getCollection();
           $customerCollection->addFieldToFilter('phone_number',['=' => $number]);
           return ($customerCollection->count()==1) ? true : false;
       }
       catch (\Exception $e)
       {
          return false;
       }

     }

    public function isEmailExists($email)
    {
        try
        {
            $customerFactory = $this->customerFactory->create();
            $customerCollection = $customerFactory->getCollection();
            $customerCollection->addFieldToFilter('email',['=' => $email]);
            return ($customerCollection->count()==1) ? true : false;
        }
        catch (\Exception $e)
        {
            return false;
        }

    }

    public function checkAccountStatus($fieldValue,$checkWith)
    {
        try{
            if (!empty($fieldValue) && !empty($checkWith)) {
                $formatCheck = ($checkWith=='phoneNumber') ? $this->isPhoneNumber($fieldValue) : $this->isEmail($fieldValue);
                $dbCheck = ($checkWith=='phoneNumber') ? $this->isPhoneNumberExists($fieldValue) : $this->isEmailExists($fieldValue);
                if($formatCheck) {
                    if($dbCheck) {
                        return $this->success(__("$fieldValue is already exists with us"),[ 'status' => 1]);
                    } else {
                        return $this->success(__("You are not registered with us. Please sign up."),[ 'status' => 0 ]);
                    }
                } else {
                    return $this->error(__("Invalid format $checkWith : $fieldValue"));
                }
            } else {
                return $this->error(__("All parameters are required"));
            }

        } catch (\Exception $e) {
            return $this->error(__("Something went wrong".$e->getMessage()));
        }
    }
}
