<?php

namespace Froogal\Magento\Helper;

class LoyaltyHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Froogal\Magento\Model\LoyaltyCoupons $loyaltyCoupons,
        \Froogal\Magento\Model\Config $config,
        \Psr\Log\LoggerInterface $logger,
        \Froogal\Magento\Model\LoyaltyApi $LoyaltyApi,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\Session $session
    ) {
        $this->loyaltyApi = $LoyaltyApi;
        $this->customerRepository = $customerRepository;
        $this->customerSession = $session;
        $this->logger = $logger;
        $this->loyaltyCoupons = $loyaltyCoupons;
        $this->config = $config;
        parent::__construct($context);
    }


    public function resetCoupon($quote)
    {
        $quoteId = $quote->getId();
        $CouponsModuleEnabled = $this->config->isLoyaltyCouponsModuleEnabled();
        $loyaltyCouponDiscount = (int)$quote->getLoyaltyCouponDiscount();

        if ($CouponsModuleEnabled) {

            if ($quoteId && $loyaltyCouponDiscount) {
                $this->loyaltyCoupons->release($quoteId);
            }
        } else {
            $this->logger->info('Coupons module not enabled');
        }
    }


    public function redeemCoupon($quote)
    {
        $quoteId = $quote->getId();
        $CouponsModuleEnabled = $this->config->isLoyaltyCouponsModuleEnabled();
        $loyaltyCouponDiscount = (int)$quote->getLoyaltyCouponDiscount();

        if ($CouponsModuleEnabled) {

            if ($quoteId && $loyaltyCouponDiscount) {
                $this->loyaltyCoupons->redeem($quoteId);
            }
        } else {
            $this->logger->info('Coupons module not enabled');
        }
    }

    public function getCustomer()
    {
        try {
            $customerId = $this->customerSession->getCustomerId();
            return $customerId ? $this->customerRepository->getById($customerId) : null;
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            die;
        }
        return null;
    }


}
