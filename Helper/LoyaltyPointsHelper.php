<?php

namespace Froogal\Magento\Helper;

class LoyaltyPointsHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    public $loyaltyApi;

    public $customerRepository;

    public $config;

    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Froogal\Magento\Model\Config $config,
        \Froogal\Magento\Model\LoyaltyApi $LoyaltyApi,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->loyaltyApi = $LoyaltyApi;
        $this->customerRepository = $customerRepository;
        $this->config = $config;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

   

    

    public function estimateLoyaltyPoints($price, $defaultLoyaltyAccumulationRate = 0.05, $customerId = null)
    {
       
        $customer = null;
        if($customerId)
        {
            $customer = $this->customerRepository->getById($customerId); 
        }
        

        if (!$customer) {
            return $this->calculateLoyaltyPoints($price, $defaultLoyaltyAccumulationRate);
        }

        $phoneNumberAttr = $customer->getCustomAttribute('phone_number');

        if (!$phoneNumberAttr) {
            return $this->calculateLoyaltyPoints($price, $defaultLoyaltyAccumulationRate);
        }

        $phoneNumber = $phoneNumberAttr->getValue();

        if (!$phoneNumber) {
            return $this->calculateLoyaltyPoints($price, $defaultLoyaltyAccumulationRate);
        }
        $response = $this->loyaltyApi->getLoyaltyProfile($phoneNumber);
        $success = $response['success'] ?? false;
        if ($success) {
            $loyaltyAccumulationRate = $response['loyaltyAccumulationRate'] ? $response['loyaltyAccumulationRate']/100 : 0;
            return $this->calculateLoyaltyPoints($price, $loyaltyAccumulationRate);
        }
        return $this->calculateLoyaltyPoints($price, $defaultLoyaltyAccumulationRate);
    }

    public function calculateLoyaltyPoints($price, $loyaltyAccumulationRate = 0.05)
    {
        return round($price * $loyaltyAccumulationRate);
    }

    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }
}
