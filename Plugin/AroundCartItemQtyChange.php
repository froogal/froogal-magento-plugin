<?php


namespace Froogal\Magento\Plugin;

class AroundCartItemQtyChange
{
    protected $loyaltyHelper;

    public function __construct(
        \Froogal\Magento\Helper\LoyaltyHelper $loyaltyHelper
    ) {
        $this->loyaltyHelper = $loyaltyHelper;
    }

    public function aroundSetQty(\Magento\Quote\Model\Quote\Item $subject,\Closure $proceed, $qty)
    {
        $oldQty = $subject->getQty();
        $result = $proceed($qty);
        $quoteId = $result->getQuoteId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $quote = $objectManager->create('Magento\Quote\Model\Quote')->load($quoteId);
        $qty = $result->getQty();
        try
        {
          if($oldQty != $qty)
          {
              $this->loyaltyHelper->resetCoupon($quote);
          }
        }
        catch (\Exception $e){
        }

        return $result;
    }
}