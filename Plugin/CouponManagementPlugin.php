<?php


namespace Froogal\Magento\Plugin;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\CouponManagement;
use Magento\Setup\Module\Dependency\Parser\Composer\Json;

class CouponManagementPlugin
{
    protected $quoteRepository;

    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Froogal\Magento\Model\LoyaltyCoupons $loyaltyCoupons,
        \Froogal\Magento\Model\Config $config
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->loyaltyCoupons = $loyaltyCoupons;
        $this->config = $config;

    }

    /**
     * @param CouponManagement $subject
     * @param \Closure $proceed
     * @param $cartId
     * @param $couponCode
     * @return bool
     * @throws NoSuchEntityException
     */
    public function aroundSet(CouponManagement $subject, \Closure $proceed, $cartId, $couponCode)
    {
        $CouponsModuleEnabled = $this->config->isLoyaltyCouponsModuleEnabled();
        if ($CouponsModuleEnabled) {
           
            $quote = $this->quoteRepository->getActive($cartId);
            
            if (!$quote->getItemsCount()) {
                throw new NoSuchEntityException(__('The "%1" Cart doesn\'t contain products.', $cartId));
            }
            if (!$quote->getStoreId()) {
                throw new NoSuchEntityException(__('Cart isn\'t assigned to correct store'));
            }
           
            $response = $this->loyaltyCoupons->hold($quote->getId(),$couponCode);
            
            $success = $response["success"] ?? false;
           
            // echo json_encode($response);
            //     die;
            if($success)
            {
                return true;
            }
            else
            {
                $errors = $response['errors'] ?? [];
                $status = $errors['status'] ?? '';
                if($status == 'COUPON_NOT_FOUND')
                {
                   return $proceed($cartId,$couponCode);
                }
                else
                {
                    $message = $response["message"] ?? 'Something went wrong';
                    throw new NoSuchEntityException(__($message));
                }
            }
        }
        else
        {
            return $proceed($cartId,$couponCode);
        }
    }

    public function aroundRemove(CouponManagement $subject, \Closure $proceed, $cartId)
    {
        $CouponsModuleEnabled = $this->config->isLoyaltyCouponsModuleEnabled();
        if ($CouponsModuleEnabled) {

            $quote = $this->quoteRepository->getActive($cartId);
            if (!$quote->getItemsCount()) {
                throw new NoSuchEntityException(__('The "%1" Cart doesn\'t contain products.', $cartId));
            }
            $response = $this->loyaltyCoupons->release($quote->getId());
            $success = $response["success"] ?? false;
            if($success)
            {
                return true;
            }
            else
            {
                $errors = $response['errors'] ?? [];
                $status = $errors['status'] ?? 'notFound';
                if($status == 'notFound')
                {
                    return $proceed($cartId);
                }
                else
                {
                    $message = $response["message"] ?? 'Something went wrong';
                    throw new NoSuchEntityException(__($message));
                }
            }
        }
        else
        {
            return $proceed($cartId);
        }
    }
}