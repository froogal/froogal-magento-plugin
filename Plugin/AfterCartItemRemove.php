<?php


namespace Froogal\Magento\Plugin;

class AfterCartItemRemove
{
    protected $loyaltyHelper;

    public function __construct(
        \Froogal\Magento\Helper\LoyaltyHelper $loyaltyHelper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->loyaltyHelper = $loyaltyHelper;
        $this->logger = $logger;
    }

    public function afterRemoveItem(\Magento\Quote\Model\Quote $quote, $result)
    {
        try {
            if($quote->getIsChanged())
            {
                $this->loyaltyHelper->resetCoupon($quote);
            }
        }
        catch (\Exception $e){
            $this->logger->info($e->getMessage());
        }

        return $result;
    }
}