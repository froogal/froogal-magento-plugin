<?php
 
namespace Froogal\Magento\Plugin;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\CustomerExtractor;



class CreatePostPlugin
{

    /**
     * @var \Magento\Customer\Model\CustomerExtractor
     */
    protected $customerExtractor;

    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Session $customerSession,
        AccountManagementInterface $accountManagement,
        CustomerExtractor $customerExtractor,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
       
        $this->customerRepository = $customerRepository;
        $this->_customerFactory = $customerFactory;
        $this->_customerSession = $customerSession;
        $this->accountManagement = $accountManagement;
        $this->customerExtractor = $customerExtractor;
        $this->_storeManager = $storeManager;
    }
 
    public function afterExecute(
        \Magento\Customer\Controller\Account\CreatePost $subject,
        $result)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $currentWebsiteId = $this->_storeManager->getStore()->getWebsiteId();

        $CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
        $CustomerModel->setWebsiteId($currentWebsiteId); 
        
        $subjectRequest = $subject->getRequest();
        $customer = $this->customerExtractor->extract('customer_account_create', $subjectRequest);
        $referralCode = $subject->getRequest()->getParam('referral_code');
        $phoneNumber = $subject->getRequest()->getParam('phone_number');
        $email = $subject->getRequest()->getParam('email');
        $customerData = $CustomerModel->loadByEmail($email);
        
        if(!empty($customerData->getData('email')))
        {
            $customerId = $customerData->getId();
        }     
        $customer = $this->customerRepository->getById($customerId);
        $phoneNumber = $subject->getRequest()->getParam('froogal_phone_number') ?? null;
        $referralCode = $subject->getRequest()->getParam('froogal_referral_code') ?? null;
        $customer->setCustomAttribute('froogal_phone_number', $phoneNumber);
        $customer->setCustomAttribute('froogal_referral_code', $referralCode);
        $this->customerRepository->save($customer); 
         
        return $result;
    }
 
}