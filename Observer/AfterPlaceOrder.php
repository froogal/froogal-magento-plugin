<?php

namespace Froogal\Magento\Observer;

use Magento\Framework\Event\ObserverInterface;
use GuzzleHttp\Client;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Zend\Log\Writer\Stream;
use Zend\Log\Logger;
class AfterPlaceOrder implements ObserverInterface
{

    private $productRepository;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CustomerRepositoryInterface $customerRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->client = new Client();
        $this->scopeConfig = $scopeConfig;
        $this->_customerRepository = $customerRepository;
        $this->productRepository = $productRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/cashfree-so-generation.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $syncOrder = $this->scopeConfig->getValue(
            'myorders_configuration/myorders/syncOrder',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
      
        if($syncOrder)
        {
        $orderIds = $observer->getEvent()->getData('order_ids');
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getData('order');
        
       

        if ($order->getCustomerId()) {
            $customer = $this->_customerRepository->getById($order->getCustomerId());
            if ($customAttribute = $customer->getCustomAttribute('phone_number')) {
                $phoneNumber = $customAttribute->getValue();
            }
        } else {
            $phoneNumber = $order->getShippingAddress()->getTelephone();
        }


        $orderItems = $order->getAllItems();
        $itemData = [];

        foreach ($orderItems as $item) {
            if (!$item->isDeleted() && !$item->getParentItem()) {
                $product = $item->getProduct();
                $itemData[] = [
                    "category" => "glow",
                    "productCode" => $product->getSku(),
                    "name" => $product->getName(),
                    "rate" => $product->getFinalPrice(),
                    "quantity" => $item->getQtyOrdered()
                ];
            }
        }
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $methodTitle = $method->getTitle()??'cashfree';
        $paymentData = [[
            "name" => $methodTitle,
            "amount" => $order->getGrandTotal()
        ]];
        $shippingAddress = $order->getShippingAddress();
        $logger->info('shipping address'.json_encode($shippingAddress->getData()));
        if ($shippingAddress) {
        $city = $shippingAddress->getCity();
        $state = $shippingAddress->getRegion();
        $pinCode = $shippingAddress->getPostcode();
        $logger->info('city='.$city. 'state=' .$state. 'pincode=' .$pinCode);
        $order->setData('region_state', $state);
        $order->setData('city', $city);
        $order->setData('pin_code', $pinCode);
        }
        $order->setData('phone_number', $phoneNumber);        
        $order->save();
        $logger->info('order shipping address'.json_encode($order->getData()));
        $orderData = [
            "phoneNumber" => $phoneNumber,
            "name" => $order->getCustomerName(),
            "email" => $order->getCustomerEmail(),
            "referenceNumber" => $order->getEntityId(),
            "amount" => $order->getGrandTotal(),
            "discount" => $order->getDiscountAmount(),
            "createdDate" => $order->getCreatedAt(),
            "items" => $itemData,
            "payments" => $paymentData
        ];

        $url = $this->scopeConfig->getValue(
            'myorders_configuration/myorders/apiUrl',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $accessToken = $this->scopeConfig->getValue(
            'myorders_configuration/myorders/merchantSecret',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $params = [
            "order" => $orderData,
            "accessToken" => $accessToken
        ];
        try {
            $response = $this->client->request('POST', "{$url}/magento-order/store", [
                'json' => $params,
            ]);

            $data = json_decode($response->getBody(), true);
        } catch (\Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();
        }
    }
}
}
