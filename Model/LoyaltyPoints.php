<?php

namespace Froogal\Magento\Model;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Quote\Model\QuoteIdToMaskedQuoteIdInterface;


class LoyaltyPoints extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var QuoteIdToMaskedQuoteIdInterface
     */
    private $quoteIdToMaskedQuoteId;

    public $loyaltyPointsHelper;

    public $product;
    public $quoteFactory;

    protected $quoteIdMaskFactory;

    protected $cart;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Froogal\Magento\Helper\LoyaltyPointsHelper $loyaltyPointsHelper,
        \Magento\Catalog\Model\Product $product,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Froogal\Magento\Helper\ResponseHelper $responseHelper,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        DataObjectProcessor $dataObjectProcessor,
        \Froogal\Magento\Model\LoyaltyApi $LoyaltyApi,
        \Froogal\Magento\Model\Config $config,
        \Magento\Quote\Model\QuoteRepository $quoteModelRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        QuoteIdToMaskedQuoteIdInterface $quoteIdToMaskedQuoteId

    )
    {

        $this->loyaltyPointsHelper = $loyaltyPointsHelper;
        $this->product = $product;
        $this->quoteFactory = $quoteFactory;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->cart = $cart;
        $this->responseHelper = $responseHelper;
        $this->quoteRepository = $quoteRepository;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->loyaltyApi = $LoyaltyApi;
        $this->config = $config;
        $this->quoteModelRepository = $quoteModelRepository;
        $this->customerRepository = $customerRepository;
        $this->quoteIdToMaskedQuoteId = $quoteIdToMaskedQuoteId;

        parent::__construct($context);
    }



    /**
     * Returns earned points
     *
     * @api
     * @param mixed $params  
     * @return array.
     */
    public function getCheckoutPoints($params)
    {
        $sku = $params['sku'] ?? null;
        $customOptionId = $params['customOptionId'] ?? null;
        $optionValue = $params['optionValue'] ?? null;
        $customerId = $params['customerId'] ?? null;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $quoteId = $params['cartId'] ?? null;
        $cartId = null;
        if ($quoteId) {
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($quoteId, 'masked_id');
            $cartId = $quoteIdMask->getQuoteId();

            if ($cartId) {
                $quote = $this->quoteFactory->create()->loadByIdWithoutStore($cartId);
                $price = $quote->getGrandTotal();
            }
        } else {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productId = $objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($sku);

            $product = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
            $finalPrice = $product->getFinalPrice();
            if ($product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                $regularPrice = $product->getPriceInfo()->getPrice('regular_price');
                $price = $regularPrice->getMaxRegularAmount()->getValue();
            } 
            else if($customOptionId){
                $price = $this->getOptionPrice($product,$finalPrice,$customOptionId,$optionValue);
            }else {
                $price = $product->getFinalPrice();
            }
        }
        $points = $this->loyaltyPointsHelper->estimateLoyaltyPoints($price, 0.015, $customerId, $cartId);
        $points = ['success' => true, 'message' => 'earned points', 'points' => $points];
        return $points;
    }

    public function getOptionPrice($product,$finalPrice,$customOptionId,$optionValue)
    {
        if (count($product->getOptions()) > 0) {

            foreach ($product->getOptions() as $options) {
                $optionTitle = strtolower($options->getTitle());
                if ($optionTitle == 'ring size') {
                    $optionData = $options->getValues();
                    foreach ($optionData as $data) {

                        $size = $data->getTitle();                            
                        $optionTypeId = $data->getData('option_type_id');
                        if($optionTypeId == $optionValue)
                        {
                            $customOptionPrice = $data->getPrice();
                            $finalPrice = $finalPrice + $customOptionPrice;
                        }                           

                    }
                }
            }
        }
        return $finalPrice;
    }

    /**
     * Returns balance points
     *
     * @api
     * @param int $customerId  
     * @return array.
     */
    public function getLoyaltyDetails($customerId)
    {
        $enabled = $this->config->isLoyaltyPointsModuleEnabled();
        $customer = $this->customerRepository->getById($customerId);
        $phoneNumber = $customer->getCustomAttribute('phone_number')->getValue();
        if(!empty($phoneNumber))
        {
            $response = $this->loyaltyApi->getDetails($phoneNumber);
            $success = $response['success'] ?? false;
            if($success)
            {
                $eligibleLoyaltyPoints = $response['eligibleLoyaltyPoints'] ?? 0;
                $loyaltyRedemptionRate = $response['loyaltyRedemptionRate'] ?? 0;

                $data['eligibleLoyaltyPoints'] = (int)$eligibleLoyaltyPoints;
                $data['loyaltyRedemptionRate'] = (int)$loyaltyRedemptionRate;

                return $this->responseHelper->success('Customer loyalty details',$data);
            }
            else
            {
                $message = $response['message'] ?? 'Something went wrong';
                return $this->responseHelper->error($message);
            }
        }
        else
        {
            return $this->responseHelper->error("Customer don't have phone number");
        }
    }

    /**
     * Get cart details
     *
     * @api
     * @param int $customerId 
     * @return array.
     */
    public function getLoyaltyCartDetails($customerId)
    {
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/loyaltypoints.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $enabled = $this->config->isLoyaltyPointsModuleEnabled();
        if($enabled)
        {
            $customer = $this->customerRepository->getById($customerId);
            $quoteRepository = $this->quoteRepository->getActiveForCustomer($customer->getId());
            $quoteId = $quoteRepository->getId();
            $quote = $this->quoteRepository->get($quoteId);
            $quoteMaskId = $this->getQuoteMaskId($quoteId);
            $logger->info('quoteMaskId'.$quoteMaskId);
            $loyaltyDiscount = $quote->getData('loyalty_discount');
            if($loyaltyDiscount > 0)
            {
                $checkforValidatePoints = $this->loyaltyPointsRelease($customerId,$quoteMaskId);
                $quote->setData('loyalty_discount', 0);
                $quote->setData('redeemed_loyalty_points', 0);
                $quote->save();
            
                $checkforBalancePoints = $this->loyaltyPointsHold($customerId,$loyaltyDiscount);
                $logger->info('checkforBalancePoints'.json_encode($checkforBalancePoints));
                $loyaltyDiscount = $checkforBalancePoints['data']['invoiceDiscount'] ?? 0;
                $pointsUsed = $checkforBalancePoints['data']['pointsUsed'] ?? 0;
                $quote->setData('loyalty_discount', $loyaltyDiscount);
                $quote->setData('redeemed_loyalty_points', $pointsUsed);
                $quote->save();
            }
            $data = [];
            if($loyaltyDiscount)
            {
                $data = [
                    'loyalty_discount' => $quote->getData('loyalty_discount'),
                    'coupon_code' => $quote->getData('coupon_code'),
                    'redeemed_loyalty_points' => $quote->getData('redeemed_loyalty_points'),
                    'loyalty_coupon_discount' => $quote->getData('loyalty_coupon_discount'),
                    'loyalty_coupon_code' => $quote->getData('loyalty_coupon_code'),
                    'loyalty_coupon_title' => $quote->getData('loyalty_coupon_title'),
                    'loyalty_coupon_discount_type' => $quote->getData('loyalty_coupon_discount_type'),
                    'message' => 'Points are already redeemed on the current bill',
                ];
            }
            $data['grand_total'] = $quote->getGrandTotal();
            $data['entity_id'] = $quote->getId();


            $quote->setTotalsCollectedFlag(false)->collectTotals();

            return $this->responseHelper->success('Loyalty cart details',$data);

        }
    }

    /**
     * Returns earned points
     *
     * @api
     * @param string $quoteId  
     * @return array.
     */
    public function loyaltyPointsValidate($quoteId)
    {
        $enabled = $this->config->isLoyaltyPointsModuleEnabled();
        if($enabled)
        {
            $cartId = $this->getQuoteId($quoteId);
            $quote = $this->quoteRepository->get($cartId);
            $itemsData= $quote->getItems();
            $items = array();
            $itemTotalAmount = 0;
            foreach ($itemsData as $item) {
                $parseItem = array();
                $parseItem['name'] = $item['name'] ?? '' ;
                $parseItem['id'] = $item['sku'] ?? '';
                $parseItem['quantity'] = $item['qty'] ?? 1;
                $parseItem['rate'] = $item['price'] ?? 0;
                $itemTotalAmount = $itemTotalAmount + ( $parseItem['rate'] * $parseItem['quantity'] );
                $items[] = $parseItem;
            }
            $customer = $quote->getCustomer();
           
            $phoneNumber = null;
            if($customer)
            {
                $phoneNumber = $this->getCustomerPhoneNumber($customer);
            }
            $trackingId = $cartId;
            $cart = array();
            $cart['totalAmount'] = $itemTotalAmount;
            $cart['items'] = $items;
            $storeCode = 'STORE_'.$this->loyaltyPointsHelper->getStoreId();
            $response = $this->loyaltyApi->validatePoints($phoneNumber,$storeCode,$trackingId,$cart);
            $success = $response['success'] ?? false;
            if($success)
            {
                $loyaltyPointsDiscount = $response['availablePointsValue'] ?? 0;
                $pointsUsed = $response['availablePointsToRedeem'] ?? 0;

                // $quote = $this->quoteModelRepository->get($quoteId);
                
                $data['availablePoints'] = (int)$response['availablePoints'];
                $data['availablePointsToRedeem'] = (int)$response['availablePointsToRedeem'];
                $data['pointsDiscount'] = (int)$loyaltyPointsDiscount;

                return $this->responseHelper->success('Validated Points',$data);
            }
            else
            {
                $message = $response['message'] ?? 'Something went wrong';
                // $data = $response['status'] ?? 'notFound';
                
                return $this->responseHelper->error($message,$response);
            }
        }
        else
        {
            return $this->responseHelper->error("Loyalty coupon module not enabled");
        }
    
    }

   
    /**
     * @param int $customerId
     * @param int $points
     * @return array
     * @api
     */
    public function loyaltyPointsHold($customerId,$points)
    {
        $enabled = $this->config->isLoyaltyPointsModuleEnabled();
        if($enabled)
        {
            $customer = $this->customerRepository->getById($customerId);
            $quoteRepository = $this->quoteRepository->getActiveForCustomer($customer->getId());
            $quoteId = $quoteRepository->getId();
            $quote = $this->quoteRepository->get($quoteId);
            $itemsData= $quote->getItems();
            $items = array();
            $itemTotalAmount = 0;
            foreach ($itemsData as $item) {
                //$item = $this->dataObjectProcessor->buildOutputDataArray($d, 'Magento\Quote\Api\Data\CartItemInterface');
                $parseItem = array();
                $parseItem['name'] = $item['name'] ?? '' ;
                $parseItem['id'] = $item['sku'] ?? '';
                $parseItem['quantity'] = $item['qty'] ?? 1;
                $parseItem['rate'] = $item['price'] ?? 0;
                $itemTotalAmount = $itemTotalAmount + ( $parseItem['rate'] * $parseItem['quantity'] );
                $items[] = $parseItem;
            }
            $phoneNumber = $customer->getCustomAttribute('phone_number')->getValue();
            if(!empty($phoneNumber))
            {
                $trackingId = $quoteId;
                $cart = array();
                $cart['totalAmount'] = $itemTotalAmount;
                $cart['items'] = $items;
                $storeCode = 'STORE_'.$this->loyaltyPointsHelper->getStoreId();
                $response = $this->loyaltyApi->holdPoints($phoneNumber,$storeCode,$points,$trackingId,$cart);
                $success = $response['success'] ?? false;
                if($success)
                {
                    $loyaltyDiscount = $response['invoiceDiscount'] ?? 0;
                    $pointsUsed = $response['pointsUsed'] ?? 0;

                    $quote = $this->quoteModelRepository->get($quoteId);
                    $quote->setData('loyalty_discount', $loyaltyDiscount);
                    $quote->setData('redeemed_loyalty_points', $pointsUsed);
                    $quote->save();
                    $quote->setTotalsCollectedFlag(false)->collectTotals();
                    $data['invoiceDiscount'] = (int)$loyaltyDiscount;
                    $data['pointsUsed'] = (int)$pointsUsed;
                    return $this->responseHelper->success('Hold customer loyalty points',$data);
                }
                else
                {
                    $message = $response['message'] ?? 'Something went wrong';
                    return $this->responseHelper->error($message);
                }
            }
            else
            {
                return $this->responseHelper->error("Customer don't have phone number");
            }
        }
        else
        {
            return $this->responseHelper->error("Loyalty points module not enabled");
        }
    }

    /**
     * @param int $customerId
     * @param string $quoteId
     * @return array
     * @api
     */
    public function loyaltyPointsRelease($customerId,$quoteId)
    {
        $enabled = $this->config->isLoyaltyPointsModuleEnabled();
        if($enabled)
        {
            $cartId = $this->getQuoteId($quoteId);
            $customer = $this->customerRepository->getById($customerId);
            $phoneNumber = $customer->getCustomAttribute('phone_number')->getValue();
            $trackingId = $cartId;
            if(!empty($phoneNumber))
            {
                $quote = $this->quoteModelRepository->get($cartId);
                $quote->setData('loyalty_discount', 0);
                $quote->setData('redeemed_loyalty_points', 0);
                $quote->save();
                $quote->setTotalsCollectedFlag(false)->collectTotals();
                $storeCode = 'STORE_'.$this->loyaltyPointsHelper->getStoreId();
                $response = $this->loyaltyApi->releasePoints($phoneNumber,$storeCode,$trackingId);
                $success = $response['success'] ?? false;
                if($success)
                {
                    $pointsReleased = $response['pointsReleased'] ?? 0;
                    $eligibleLoyaltyPoints = $response['eligibleLoyaltyPoints'] ?? 0;

                    $data['pointsReleased'] = (int)$pointsReleased;
                    $data['eligibleLoyaltyPoints'] = (int)$eligibleLoyaltyPoints;
                    return $this->responseHelper->success('Release customer loyalty points',$data);
                }
                else
                {
                    $message = $response['message'] ?? 'Something went wrong';
                    return $this->responseHelper->error($message);
                }
            }
            else
            {
                return $this->responseHelper->error("Customer don't have phone number");
            }
        }
        else
        {
            return $this->responseHelper->error("Loyalty points module not enabled");
        }
    }
    

    public function getCustomerPhoneNumber($customer)
    {
        $phoneNumber="";
        $customerId = $customer->getId();
        if($customerId)
        {
            $customer = $this->customerRepository->getById($customerId);
            $phoneNumber = $customer->getCustomAttribute('phone_number')->getValue();
        }
        return $phoneNumber;
    }

    public function getQuoteId($quoteId)
    {
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($quoteId, 'masked_id');       
        $cartId = $quoteIdMask->getQuoteId();
        return $cartId;
    }
        
    public function getQuoteMaskId($quoteId)
    {
        $maskedId = null;
        try {
            $maskedId = $this->quoteIdToMaskedQuoteId->execute($quoteId);
        } catch (\Exception $exception) {
            throw new LocalizedException(__('Current user does not have an active cart.'));
        }

        return $maskedId;
    }

    


}