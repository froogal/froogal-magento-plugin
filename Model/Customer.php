<?php

namespace Froogal\Magento\Model;

use GuzzleHttp\Client;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\CacheInterface;




class Customer extends \Magento\Framework\Model\AbstractModel
 implements \Froogal\Magento\Api\CustomerInterface
{
  

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    protected $customerSession;
   

    public function __construct(
        ScopeConfigInterface $scopeConfigInterface,
        CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Froogal\Magento\Helper\Helpers $Helpers,
        \Froogal\Magento\Model\Config $config,
        CacheInterface $cache

    ) {
        $this->client = new Client();
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->customerRepository = $customerRepository;
        $this->_customerFactory = $customerFactory;
        $this->customerSession = $customerSession; 
        $this->helper = $Helpers;
        $this->cacheStorage = $cache;
        $this->config = $config;

        

    }

    protected function getCustomerId()
    {
        $customer = $this->customerSession;
        $customerId = $customer->getCustomer()->getId();
        return $customerId;
    }

    /**
     *  Customer
     * @param string $phoneNumber
     * @param string $authToken
     * @return array
     */
    public function saveAccessToken($phoneNumber, $authToken)
    {        
            $customerId = $this->getCustomerId();        
            $customer = $this->customerRepository->getById($customerId);
            $customer->setCustomAttribute('froogal_phone_number', $phoneNumber);
            $customer->setCustomAttribute('froogal_auth_token', $authToken);
            $this->customerRepository->save($customer);
            $customer = $this->_customerFactory->create()->load($customer->getId());
            $customerData = $customer->getData();
            return $customerData;
        
    }

    public function getApiUrl()
    {
        return $this->config->getConfigData('apiUrl');
    }

     public function getCustomer($customerId)
    {
        try {         

            return $customerId ? $this->customerRepository->getById($customerId) : null;
        } catch (\Exception $exception) {
        }
        return null;
    }
    /**
     * @api
     * @param int $customerId
     * @return array
     */
     public function getAuthToken($customerId)
    {
        
        $customer = $this->getCustomer($customerId);
       
        if (!$customer) {
            return $this->helper->error('Customer Not logged In');
           }

        $phoneNumberAttr = $customer->getCustomAttribute('phone_number');

        if (!$phoneNumberAttr) {
            return null;
        }

        $phoneNumber = $phoneNumberAttr->getValue();

        if (!$phoneNumber) {
            return null;
        }

        $cacheKey = 'froogal_user_auth_token_' . $phoneNumber;
        $authToken = $this->cacheStorage->load($cacheKey);
        if ($authToken) {
            $data = [
                'authToken'=>$authToken
            ];
            return $this->helper->success("authtoken generated successfully", $data);
        }

        $apiUrl = $this->getApiUrl();
        $secret = $this->config->getConfigData('merchantSecret');

        $responseStr = (string)$this->client->post("$apiUrl/customer/generate-auth-token", [
            'json' => [
                'phoneNumber' => $phoneNumber,
            ],
            'headers' => [
                'Authorization' => 'Bearer ' . $secret,
            ],
        ])->getBody();
        $response = json_decode($responseStr, true);
        $authToken = $response['authToken'];
        $this->cacheStorage->save($authToken, $cacheKey, [], 60 * 60 * 24 * 60);
        $data = [
            'authToken'=>$authToken
        ];
        return $this->helper->success("authtoken generated successfully", $data);

    }
  
   
}
