<?php


namespace Froogal\Magento\Model;


use Froogal\Magento\Api\LoyaltyCouponsInterface;
use Magento\Framework\Reflection\DataObjectProcessor;

class LoyaltyCoupons implements LoyaltyCouponsInterface
{

    protected $loyaltyApi;

    protected $quoteModel;

    protected $responseHelper;

    protected $customerRepository;

    protected $quoteRepository;

    protected $dataObjectProcessor;

    protected $quoteModelRepository;

    private $logger;

    public function __construct(
        \Froogal\Magento\Model\LoyaltyApi $LoyaltyApi,
        \Froogal\Magento\Helper\ResponseHelper $responseHelper,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Quote\Model\QuoteRepository $quoteModelRepository,
        DataObjectProcessor $dataObjectProcessor,
        \Froogal\Magento\Model\Config $config,
        \Magento\Quote\Model\Quote $quote,
        \Psr\Log\LoggerInterface $logger)
    {
        $this->loyaltyApi = $LoyaltyApi;
        $this->config = $config;
        $this->quoteModel = $quote;
        $this->responseHelper = $responseHelper;
        $this->customerRepository = $customerRepository;
        $this->quoteRepository = $quoteRepository;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->quoteModelRepository = $quoteModelRepository;
        $this->logger = $logger;

    }

    /**
     * @param int $customerId
     * @param int $points
     * @return array
     * @api
     */
    public function hold($quoteId,$couponCode)
    {
        $enabled = $this->config->isLoyaltyCouponsModuleEnabled();
        if($enabled)
        {
            $quote = $this->quoteRepository->get($quoteId);
            $itemsData= $quote->getItems();
            $items = array();
            $itemTotalAmount = 0;
            foreach ($itemsData as $d) {
                $item = $this->dataObjectProcessor->buildOutputDataArray($d, 'Magento\Quote\Api\Data\CartItemInterface');
                $parseItem = array();
                $parseItem['name'] = $item['name'] ?? '' ;
                $parseItem['id'] = $item['sku'] ?? '';
                $parseItem['quantity'] = $item['qty'] ?? 1;
                $parseItem['rate'] = $item['price'] ?? 0;
                $itemTotalAmount = $itemTotalAmount + ( $parseItem['rate'] * $parseItem['quantity'] );
                $items[] = $parseItem;
            }
            $customer = $quote->getCustomer();
            $phoneNumber = null;
            if($customer)
            {
                $phoneNumber = $this->getCustomerPhoneNumber($customer);
            }
            $trackingId = $quoteId;
            $cart = array();
            $cart['totalAmount'] = $itemTotalAmount;
            $cart['items'] = $items;
            $response = $this->loyaltyApi->holdCoupon($phoneNumber,$couponCode,$trackingId,$cart);
            $success = $response['success'] ?? false;
            if($success)
            {
                $loyaltyCouponDiscount = $response['invoiceDiscount'] ?? 0;
                $loyaltyCouponDiscountType = $response['invoiceDiscountType'] ?? 'flat';

                $loyaltyCouponDiscountTitle = $response['title'];

                $quote = $this->quoteModelRepository->get($quoteId);
                $quote->setData('loyalty_coupon_discount', $loyaltyCouponDiscount);
                $quote->setData('loyalty_coupon_discount_type', $loyaltyCouponDiscountType);
                $quote->setData('loyalty_coupon_code', $couponCode);
                $quote->setData('loyalty_coupon_title', $loyaltyCouponDiscountTitle);
                $quote->save();
                $quote->setTotalsCollectedFlag(false)->collectTotals();
                $data['invoiceDiscount'] = (int)$loyaltyCouponDiscount;
                $data['loyaltyCouponDiscountType'] = $loyaltyCouponDiscountType;
                return $this->responseHelper->success('Hold coupon code',$data);
            }
            else
            {
                $message = $response['message'] ?? 'Something went wrong';
                $data['status'] = $response['status'] ?? 'notFound';
                return $this->responseHelper->error($message,$data);
            }
        }
        else
        {
            return $this->responseHelper->error("Loyalty coupon module not enabled");
        }
    }

    /**
     * @param int $customerId
     * @return array
     * @api
     */
    public function redeem($quoteId)
    {
        $enabled = $this->config->isLoyaltyCouponsModuleEnabled();
        if($enabled)
        {
            $quote = $this->quoteModelRepository->get($quoteId);
            $customer = $quote->getCustomer();
            $phoneNumber = null;
            $loyaltyCouponCode = $quote->getLoyaltyCouponCode();
            if($customer)
            {
                $phoneNumber = $this->getCustomerPhoneNumber($customer);
            }
            $trackingId = $quoteId;
            if(!empty($loyaltyCouponCode))
            {
                $response = $this->loyaltyApi->redeemCoupon($phoneNumber,$trackingId,$loyaltyCouponCode);
                $success = $response['success'] ?? false;
                if($success)
                {
                    return $this->responseHelper->success('Success fully redeemed');
                }
                else
                {
                    $message = $response['message'] ?? 'Something went wrong';
                    return $this->responseHelper->error($message);
                }
            }
            else
            {
                return $this->responseHelper->error("Coupon code required");
            }
        }
        else
        {
            return $this->responseHelper->error("Loyalty coupon module not enabled");
        }
    }

    /**
     * @param int $trackingId
     * @return array
     * @api
     */
    public function release($quoteId)
    {
        $enabled = $this->config->isLoyaltyCouponsModuleEnabled();
        if($enabled)
        {
            $quote = $this->quoteModelRepository->get($quoteId);
            $customer = $quote->getCustomer();
            $loyaltyCouponCode = $quote->getLoyaltyCouponCode();

            $phoneNumber = null;
            if($customer)
            {
                $phoneNumber = $this->getCustomerPhoneNumber($customer);
            }
            $trackingId = $quoteId;
            if(!empty($loyaltyCouponCode))
            {
                $quote = $this->quoteModelRepository->get($quoteId);
                $quote->setData('loyalty_coupon_discount', 0);
                $quote->setData('loyalty_coupon_discount_type', 0);
                $quote->setData('loyalty_coupon_code', '');
                $quote->setData('loyalty_coupon_title', '');
                $quote->save();

                $response = $this->loyaltyApi->releaseCoupon($phoneNumber,$trackingId,$loyaltyCouponCode);
                $success = $response['success'] ?? false;
                if($success)
                {
                    return $this->responseHelper->success('Release coupon code',[]);
                }
                else
                {
                    $message = $response['message'] ?? 'Something went wrong';
                    $data['status'] = $response['status'] ?? 'notFound';
                    return $this->responseHelper->error($message,$data);
                }
            }
            else
            {
                return $this->responseHelper->error("Coupon code required");
            }

        }
        else
        {
            return $this->responseHelper->error("Loyalty coupon module not enabled");
        }
    }

    /**
     * @return array
     * @api
     */
    public function getCouponConfigDetails($quoteId = null)
    {
        $response = $this->loyaltyApi->getCouponConfigDetails($quoteId);
        return $this->responseHelper->success('Loyalty coupon config details',$response);
    }

    /**
     * @return array
     * @api
     */
    public function validateLoyaltyCoupon($couponCode)
    {
        $enabled = $this->config->isLoyaltyCouponsModuleEnabled();
        if($enabled)
        {
            $response = $this->loyaltyApi->validateCouponCode($couponCode);
            $success = $response['success'] ?? false;
            if($success)
            {
                return $this->responseHelper->success('Validate coupon code');
            }
            else
            {
                $message = $response['message'] ?? 'Something went wrong';
                return $this->responseHelper->error($message);
            }
        }
        else
        {
            return $this->responseHelper->error("Loyalty coupon module not enabled");
        }
    }

    public function getCustomerPhoneNumber($customer)
    {
        $phoneNumber="";
        $customerId = $customer->getId();
        if($customerId)
        {
            $customer = $this->customerRepository->getById($customerId);
            $phoneNumber = $customer->getCustomAttribute('phone_number')->getValue();
        }
        return $phoneNumber;
    }
}