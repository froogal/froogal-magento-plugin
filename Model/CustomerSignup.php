<?php

namespace Froogal\Magento\Model;

use GuzzleHttp\Client;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * @inheritdoc
 */
class CustomerSignup implements \Froogal\Magento\Api\CustomerSignupInterface
{
    const XML_PATH_SECURE_IN_FRONTEND = 'web/secure/use_in_frontend';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    protected $Helpers;

    public function __construct(
        \Froogal\Magento\Helper\Helpers $Helpers,
        Context $context,
        ScopeConfigInterface $scopeConfigInterface
    ) {
        $this->client = new Client();
        $this->helper = $Helpers;
        $this->_urlBuilder = $context->getUrlBuilder();
        $this->scopeConfig = $context->getScopeConfig();
        $this->scopeConfigInterface = $scopeConfigInterface;
    }

    /**
     * @inheritdoc
     */
    public function validateReferralCode($programId, $referralCode)
    {
        try {
            $url = "https://dashboard.froogal.ai/api/referral/v1/user/referrer";
            $response = $this->client->request("POST", $url, [
                'json' => [
                    "programId" => $programId,
                    "referrerCode" => $referralCode
                ]
            ]);

            $data = json_decode($response->getBody(), true);
            return $this->helper->success("Valdate Referral Code Data", $data);
        } catch (\Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();
            return $this->helper->error('Something went wrong', $response);
        }
    }
   
}
