<?php

namespace Froogal\Magento\Model;

use \Magento\Framework\App\Config\ScopeConfigInterface;

class Config
{
    const LOYALTY_MODULE_ENABLE= 'loyalty_active';
    const REFERRAL_MODULE_ENABLE= 'referral_active';
    const POINTS_MODULE_ENABLE= 'points_active';
    const BENEFITS_MODULE_ENABLE= 'benefits_active';
    const COUPON_MODULE_ENABLE= 'coupon_active';
    const ENV = 'loyalty_environment';
    const ACCESS_TOKEN = 'merchantSecret';
    const PROD_URL = 'apiUrl';
    const TESTING_URL= 'apiUrl';
    const MIN_ORDER_VALUE = 'min_order_value';

    const DISPLAY_LABEL = 'display_label';


    

    protected $configCode = 'froogalAscend';

    protected $pointsCode = 'loyalty_points_config';

    protected $couponsCode ='loyalty_coupons_config';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var int
     */
    protected $storeId = null;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    public function isLoyaltyPointsModuleEnabled()
    {
        return (bool) (int) $this->getFroogalAscendConfigData(self::POINTS_MODULE_ENABLE, $this->storeId);
    }

    public function isLoyaltyModuleEnabled()
    {
        return (bool) (int) $this->getFroogalAscendConfigData(self::LOYALTY_MODULE_ENABLE, $this->storeId);
    }
    
    public function isReferralModuleEnabled()
    {
        return (bool) (int) $this->getFroogalAscendConfigData(self::REFERRAL_MODULE_ENABLE, $this->storeId);
    }

    public function isBenefitsModuleEnabled()
    {
        return (bool) (int) $this->getFroogalAscendConfigData(self::BENEFITS_MODULE_ENABLE, $this->storeId);
    }

    public function isLoyaltyCouponsModuleEnabled()
    {
        return (bool) (int) $this->getCouponsConfigData(self::COUPON_MODULE_ENABLE, $this->storeId);
    }

    

    public function getMinimumOrderAmountForPoints()
    {
        return (int)$this->getFroogalAscendConfigData(self::MIN_ORDER_VALUE);
    }

    public function getMinimumOrderAmountForCoupons()
    {
        return (int)$this->getCouponsConfigData(self::MIN_ORDER_VALUE);
    }

    public function getENV()
    {
        return $this->getConfigData(self::ENV);
    }

    public function getAccessToken()
    {
        return $this->getConfigData(self::ACCESS_TOKEN);
    }

    public function getTestingUrl()
    {
        return $this->getConfigData(self::TESTING_URL);
    }

    public function getProdURL()
    {
        return $this->getConfigData(self::PROD_URL);
    }

    public function getLoyaltyLabel()
    {
        return $this->getPointsConfigData(self::DISPLAY_LABEL);
    }

    public function renderLabel($value,$label)
    {
        $search = ['{{var}}'];
        $replace = [$value];
        return str_replace($search, $replace, $label);
    }

    public function getLoyaltyCouponLabel()
    {
        return $this->getCouponsConfigData(self::DISPLAY_LABEL);
    }

    /**
     * Return url according to environment
     * @return string
     */
    public function getCgiUrl()
    {
        $env = $this->getENV();
        if ($env === 'LIVE') {
            return $this->getProdURL();
        }
        return $this->getTestingUrl();
    }


    /**
     * @param int $storeId
     * @return $this
     */
    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;
        return $this;
    }

    /**
     * Retrieve information from payment configuration
     *
     * @param string $field
     * @param null|string $storeId
     *
     * @return mixed
     */
    public function getConfigData($field, $storeId = null)
    {
        if ($storeId == null) {
            $storeId = $this->storeId;
        }

        $code = $this->configCode;
        $path = 'froogalascendconfiguration/' . $code . '/' . $field;
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }

    public function getCouponsConfigData($field, $storeId = null)
    {
        if ($storeId == null) {
            $storeId = $this->storeId;
        }

        $code = $this->couponsCode;
        
        $path = 'froogalascendconfiguration/' . $code . '/' . $field;
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }

    public function getPointsConfigData($field, $storeId = null)
    {
        if ($storeId == null) {
            $storeId = $this->storeId;
        }

        $code = $this->pointsCode;

        $path = 'froogalascendconfiguration/' . $code . '/' . $field;
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }

    public function getFroogalAscendConfigData($field, $storeId = null)
    {
        if ($storeId == null) {
            $storeId = $this->storeId;
        }

        $code = $this->pointsCode;
        
        $path = 'froogalascendconfiguration/' . $code . '/' . $field;
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }
}
