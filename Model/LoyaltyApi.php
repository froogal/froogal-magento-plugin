<?php


namespace Froogal\Magento\Model;


use GuzzleHttp\Client;

class LoyaltyApi
{
    const LOYALTY_DETAILS = '/loyalty/points/balance';
    const HOLD_POINTS = '/loyalty/points/hold';
    const RELEASE_POINTS = '/loyalty/points/cancel';
    const REDEEM_POINTS = '/loyalty/points/redeem';
    const VALIDATE_POINTS = '/loyalty/points/validate';	
    const SIGNUP_POINTS = '/loyalty/signup';

    const COUPON_HOLD = '/loyalty/coupon/hold';
    const COUPON_RELEASE = '/loyalty/coupon/release';
    const COUPON_REDEEM = '/loyalty/coupon/redeem';
    const COUPON_VALIDATE= '/loyalty/coupon/validate';

    const LOYALTY_PROFILE= '/loyalty/details';

    protected $config;

    public function __construct(Config $config,\Magento\Quote\Api\CartRepositoryInterface $quoteRepository)
    {
        $this->client = new Client();
        $this->quoteRepository = $quoteRepository;
        $this->config = $config;
    }

    public function validateCouponCode($couponCode)
    {
        $params = [ "accessToken" => $this->config->getAccessToken(), "couponCode" => $couponCode];
        return $this->triggerApiCall(static::COUPON_VALIDATE,$params);
    }
    public function getConfigDetails($quoteId)
    {
        $data =[
            "isModuleEnabled" => $this->config->isLoyaltyPointsModuleEnabled(),
            "minimumOrderAmount" => (int)$this->config->getMinimumOrderAmountForPoints(),
            "loyaltyDiscount" => 0,
            "redeemedLoyaltyPoints" => 0
        ];
        if($quoteId)
        {
            $quote = $this->quoteRepository->get($quoteId);
            $quote->getLoyaltyDiscount();
            $quote->getRedeemedLoyaltyPoints();
            $data["loyaltyDiscount"] = (int)$quote->getLoyaltyDiscount();
            $data["redeemedLoyaltyPoints"] = (int)$quote->getRedeemedLoyaltyPoints();
        }

        return $data;
    }

    public function getCouponConfigDetails($quoteId)
    {
        $data =[
            "isModuleEnabled" => $this->config->isLoyaltyCouponsModuleEnabled(),
            "minimumOrderAmount" => (int)$this->config->getMinimumOrderAmountForCoupons(),
            "loyaltyCouponDiscount" => 0,
            "loyaltyCouponCode" => '',
            "loyaltyCouponDiscountType" => '',
            "loyaltyCouponTitle" => ''
        ];
        if($quoteId)
        {
            $quote = $this->quoteRepository->get($quoteId);
            $data["loyaltyCouponDiscount"] = (int)$quote->getLoyaltyCouponDiscount();
            $data["loyaltyCouponCode"] = $quote->getLoyaltyCouponCode();
            $data["loyaltyCouponDiscountType"] = $quote->getLoyaltyCouponDiscountType();
            $data["loyaltyCouponTitle"] = $quote->getLoyaltyCouponTitle();
        }

        return $data;
    }

    public function getDetails($phoneNumber)
    {
        $params = [ "accessToken" => $this->config->getAccessToken(), "phoneNumber" => $phoneNumber];
        return $this->triggerApiCall(static::LOYALTY_DETAILS,$params);
    }

    public function holdPoints($phoneNumber,$storeCode,$points,$trackingId,$cart)	
    {	        	
        $params = [ "accessToken" => $this->config->getAccessToken(), "phoneNumber" => $phoneNumber ,"storeCode"=> $storeCode, "loyaltyPointsToRedeem" => $points, "trackingId" => $trackingId, "otpVerificationRequired" => false ,"cart" => $cart];	
        return $this->triggerApiCall(static::HOLD_POINTS,$params);	
    }

    public function customerSignupPoints($phoneNumber,$name,$email,$custermerId,$referralCode)
    {
        $params = [ "accessToken" => $this->config->getAccessToken(),
            "phoneNumber" => $phoneNumber ,
            "referralCode" => $referralCode,
            "name" => $name,
            "email" => $email,
            "customerId" => $custermerId ];
        return $this->triggerApiCall(static::SIGNUP_POINTS,$params,"POST");
    }

    public function releasePoints($phoneNumber,$storeCode,$trackingId)	
    {	
        $params = [ "accessToken" => $this->config->getAccessToken(), "phoneNumber" => $phoneNumber ,"storeCode" => $storeCode ,"trackingId" => $trackingId];	
        return $this->triggerApiCall(static::RELEASE_POINTS,$params);	
    }

    public function redeemPoints($phoneNumber,$trackingId,$invoiceReferenceId)
    {
        $params = [ "accessToken" => $this->config->getAccessToken(), "phoneNumber" => $phoneNumber ,"trackingId" => $trackingId,"invoiceReferenceId" => $invoiceReferenceId];
        return $this->triggerApiCall(static::REDEEM_POINTS,$params);
    }

    public function holdCoupon($phoneNumber,$couponCode,$trackingId,$cart)
    {
        $params = [ "accessToken" => $this->config->getAccessToken(), "phoneNumber" => $phoneNumber ,"couponCode" => $couponCode, "trackingId" => $trackingId ,"cart" => $cart];
        return $this->triggerApiCall(static::COUPON_HOLD,$params);
    }

    public function releaseCoupon($phoneNumber,$trackingId,$couponCode)
    {
        $params = [ "accessToken" => $this->config->getAccessToken(), "phoneNumber" => $phoneNumber ,"trackingId" => $trackingId, "couponCode" => $couponCode];
        return $this->triggerApiCall(static::COUPON_RELEASE,$params);
    }

    public function redeemCoupon($phoneNumber,$trackingId,$couponCode)
    {
        $params = [ "accessToken" => $this->config->getAccessToken(), "phoneNumber" => $phoneNumber ,"trackingId" => $trackingId, "couponCode" => $couponCode];
        return $this->triggerApiCall(static::COUPON_REDEEM,$params);
    }

    public function getLoyaltyProfile($phoneNumber)
    {
        $params = [ "accessToken" => $this->config->getAccessToken(), "phoneNumber" => $phoneNumber];
        return $this->triggerApiCall(static::LOYALTY_PROFILE,$params);
    }
    public function validatePoints($phoneNumber,$storeCode,$trackingId,$cart)	
    {	
        	
        $params = [ "accessToken" => $this->config->getAccessToken(), "storeCode"=>$storeCode, "loyaltyPointsToRedeem" => 100, "phoneNumber" => $phoneNumber ,"trackingId" => $trackingId,"cart"=>$cart];	
        return $this->triggerApiCall(static::VALIDATE_POINTS,$params,'POST');	
    }
    
    public function balancePoints($phoneNumber,$storeCode)	
    {	
        $params = [ "accessToken" => $this->config->getAccessToken(), "phoneNumber" => $phoneNumber ,"storeCode" => $storeCode];	
        return $this->triggerApiCall(static::VALIDATE_POINTS,$params);	
    }

    protected function triggerApiCall($url, $params = [], $method = 'GET')
    {
        try
        {
            $urlPrefix = $this->config->getCgiUrl();
            $response = $this->client->request($method, "{$urlPrefix}{$url}", [
                'json' => $params,
            ]);

            $data = json_decode($response->getBody(), true);
            return $data;
        }
        catch (\Exception $e)
        {
            $response['success'] = false;
            $response['message'] = $e->getMessage();
            return $response;
        }
    }
}