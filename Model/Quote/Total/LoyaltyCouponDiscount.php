<?php


namespace Froogal\Magento\Model\Quote\Total;


class LoyaltyCouponDiscount extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    /**
     * Collect grand total address amount
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this
     */
    protected $quoteValidator = null;

    protected $_quote;


    public function __construct(\Magento\Quote\Model\QuoteValidator $quoteValidator,\Froogal\Magento\Model\Config $config)
    {
        $this->quoteValidator = $quoteValidator;
        $this->config = $config;

    }

    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);

        if (!count($shippingAssignment->getItems())) {
            return $this;
        }
        $enabled = $this->config->isLoyaltyCouponsModuleEnabled();
        if ($enabled) {
            $LoyaltyCouponDiscount = $quote->getLoyaltyCouponDiscount() * -1;
            // $LoyaltyCouponDiscountTitle = $quote->getLoyaltyCouponTitle();
            $total->setTotalAmount('loyaltyCouponDiscount', $LoyaltyCouponDiscount);
            $total->setBaseTotalAmount('loyaltyCouponDiscount', $LoyaltyCouponDiscount);
        }
        return $this;
    }

    protected function clearValues(Address\Total $total)
    {
        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);
    }

    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        $this->_quote = $quote;
        $enabled = $this->config->isLoyaltyCouponsModuleEnabled();
        $LoyaltyCouponDiscount = (int)$quote->getLoyaltyCouponDiscount();
        $LoyaltyCouponDiscountTitle = '';
        $LoyaltyCouponDiscountTitle = $quote->getLoyaltyCouponTitle();

        if ($enabled) {
            return [
                'code' => 'loyaltyCouponDiscount',
                'title' => $this->getLabel(),
                'value' => $LoyaltyCouponDiscount * -1
            ];
        }
        else
        {
            return array();
        }

    }

    public function getLabel()
    {
        $label = $this->config->getLoyaltyCouponLabel();
        $couponCode = $this->_quote->getLoyaltyCouponCode() ?? '';
        $couponTitle = $this->_quote->getLoyaltyCouponTitle();

        if(!empty($couponTitle))
        {
            $label =  $label.'('.$couponTitle.'|'.$couponCode.')';
            return __($label);
        }
        else
        {
            return __($label);
        }
    }
}