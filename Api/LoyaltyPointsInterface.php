<?php
namespace Froogal\Magento\Api;

interface LoyaltyPointsInterface
{
   

     /**
     * Returns earned points
     *
     * @api
     * @param mixed $params  
     * @return array.
     */
    public function getCheckoutPoints($params);

    /**
     * Returns balance points
     * @param int $customerId
     * @return array
     * @api
     */
    public function getLoyaltyDetails($customerId);
    /**
     * Returns earned points
     *
     * @api
     * @param string $quoteId  
     * @return array.
     */
    public function loyaltyPointsValidate($quoteId);

    /**
     * Get Balance points
     *
     * @api
     * @param int $customerId
     * @param int $points  
     * @return array.
     */
    public function loyaltyPointsHold($customerId,$points);

    /**
     * Get Balance points
     *
     * @api
     * @param int $customerId 
     * @param string $quoteId  
     * @return array.
     */
    public function loyaltyPointsRelease($customerId,$quoteId);

    /**
     * Get cart details
     *
     * @api
     * @param int $customerId 
     * @return array.
     */
    public function getLoyaltyCartDetails($customerId);
}
