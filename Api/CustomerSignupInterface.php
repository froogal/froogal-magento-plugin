<?php
namespace Froogal\Magento\Api;

/**
 * Interface providing Logout for Customers
 *
 * @api
 * @since 100.0.2
 */
interface CustomerSignupInterface
{
    /**
     * Logout Customer
     * @param int $programId
     * @param string $referralCode
     * @return array
     */
    public function validateReferralCode($programId, $referralCode);
    
}
