<?php
namespace Froogal\Magento\Api;

interface LoyaltyCouponsInterface
{

    /**
     * @api
     * @param int $quoteId
     * @param int $couponCode
     * @return array
     */
    public function hold($quoteId,$couponCode);


    /**
     * @api
     * @param int $quoteId
     * @return array
     */

    public function redeem($quoteId);

    /**
     * @api
     * @param int $quoteId
     * @return array
     */

    public function release($quoteId);

    /**
     * @api
     * @param int|null $quoteId
     * @return array
     */

    public function getCouponConfigDetails($quoteId = null);

    /**
     * @api
     * @param string $couponCode
     * @return array
     */
    public function validateLoyaltyCoupon($couponCode);
}
