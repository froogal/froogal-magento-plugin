<?php
namespace Froogal\Magento\Api;

/**
 * Interface providing Logout for Customers
 *
 * @api
 * @since 100.0.2
 */
interface CustomerInterface
{
     /**
     *  Customer
     * @param string $phoneNumber
     * @param string $authToken
     * @return array
     */
    public function saveAccessToken($phoneNumber, $authToken);

    /**
     * @api
     * @param int $customerId
     * @return array
     */
    public function getAuthToken($customerId);
    
}
