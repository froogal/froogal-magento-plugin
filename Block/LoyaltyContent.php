<?php
namespace Froogal\Magento\Block;

use GuzzleHttp\Client;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\View\Element\Template;

class LoyaltyContent extends \Magento\Framework\View\Element\Template
{
    protected $cacheStorage;

    protected $httpClient;

    protected $scopeConfig;

    protected $customerSession;

    protected $customerRepository;

    protected $storeManager;

    public function __construct(
        Template\Context $context,        
        CacheInterface $cache,
        Client $client,
        ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Session $session,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []

    ) {
        parent::__construct($context, $data);
        $this->cacheStorage = $cache;
        $this->httpClient = $client;
        $this->scopeConfig = $scopeConfig;
        $this->customerSession = $session;
        $this->customerRepository = $customerRepositoryInterface;
        $this->_storeManager = $storeManager;
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getBaseUrl()
    {
        $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
        return $baseUrl;
    }

    public function getMerchantAccessToken()
    {
        return $this->scopeConfig->getValue('froogalascendconfiguration/froogalAscend/merchantAccesstoken');
    }

    public function getApiUrl()
    {
        return $this->scopeConfig->getValue('froogalascendconfiguration/froogalAscend/apiUrl');
    }

    public function getCustomer()
    {
        try {
            $customerId = $this->customerSession->getCustomerId();
            return $customerId ? $this->customerRepository->getById($customerId) : null;
        } catch (\Exception $exception) {
        }
        return null;
    }

    public function getAuthToken()
    {
        $customer = $this->getCustomer();

        if (!$customer) {
            return null;
        }

        $phoneNumberAttr = $customer->getCustomAttribute('phone_number');

        if (!$phoneNumberAttr) {
            return null;
        }

        $phoneNumber = $phoneNumberAttr->getValue();

        if (!$phoneNumber) {
            return null;
        }

        $cacheKey = 'froogal_user_auth_token_' . $phoneNumber;
        $authToken = $this->cacheStorage->load($cacheKey);
        if ($authToken) {
            return $authToken;
        }

        $apiUrl = $this->getApiUrl();
        $secret = $this->scopeConfig->getValue('froogalascendconfiguration/froogalAscend/merchantSecret');

        $responseStr = (string)$this->httpClient->post("$apiUrl/customer/generate-auth-token", [
            'json' => [
                'phoneNumber' => $phoneNumber,
            ],
            'headers' => [
                'Authorization' => 'Bearer ' . $secret,
            ],
        ])->getBody();
        $response = json_decode($responseStr, true);
        $authToken = $response['authToken'];
        $this->cacheStorage->save($authToken, $cacheKey, [], 60 * 60 * 24 * 60);
        return $authToken;
    }
    public function getLoyaltySdkVersion()
    {
        $loyaltySdkVersion = 'v'.$this->scopeConfig->getValue('froogalascendconfiguration/froogalAscend/loyaltyVersion');
        return $loyaltySdkVersion;
    }
    public function getReferralSdkVersion()
    {
        $referralSdkVersion = 'v'.$this->scopeConfig->getValue('froogalascendconfiguration/froogalAscend/referralVersion');
        return $referralSdkVersion;
    }
    public function getreferralProgramId()
    {
        $referralProgramId = $this->scopeConfig->getValue('froogalascendconfiguration/froogalAscend/referralProgramId');
        return $referralProgramId;
    }
}
